<?php

namespace AC\TestTechniqueBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName');
        $builder->add('lastName');
        $builder->add('movieCategories');
        $builder->add('civility', 'choice', array(
            'choices' => array(
                'mr' => 'Monsieur',
                'melle' => 'Mademoiselle',
                'Mme' => 'Madame'
            ),
            'expanded' => true,
            'required'    => true,
         ));
        $builder->add('picture', new PictureType());
        $builder->add('rates','collection', array(
            'type' => new RateType(),
            'allow_add'    => true,
            'by_reference' => false,
            'allow_delete' => true
        ));

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AC\TestTechniqueBundle\Entity\User'
        ));
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

    public function getName()
    {
        return 'app_user_registration';
    }
}