<?php

namespace AC\TestTechniqueBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('movie');
        $builder->add('rate', 'choice', array(
            'choices' => array(
                '1'   => '1',
                '2'   => '2',
                '3'   => '3',
                '4'   => '4',
                '5'   => '5',
                '6'   => '6',
                '7'   => '7',
                '8'   => '8',
                '9'   => '9',
                '10'   => '10',
            ),
            'expanded' => false,
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AC\TestTechniqueBundle\Entity\Rate',
        ));
    }

    public function getName()
    {
        return 'rates';
    }
}