<?php
namespace AC\TestTechniqueBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AC\TestTechniqueBundle\Entity\User;

class LoadUserData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    //.. $container declaration & setter
    private $container;

    public function load(ObjectManager $manager)
    {
        // Liste des choses � ajouter
        $tab = array(
            array(
                'username' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => 'admin',
                'enabled' => true,
                'role' => 'ROLE_ADMIN',
                'firstname' => 'Alexandre',
                'lastname'  => 'Canivez',
                'civility'   => 'Mr',
                'created_at' => new \DateTime(),
            ),
            array(
                'username' => 'Alex',
                'email' => 'alex@gmail.com',
                'password' => 'alex',
                'enabled' => true,
                'role' => 'ROLE_USER',
                'firstname' => 'Alexandre',
                'lastname'  => 'Canivez',
                'civility'   => 'Mr',
                'created_at' => new \DateTime(),
            ),
            array(
                'username' => 'Alex2',
                'email' => 'Alex2@gmail.com',
                'password' => 'alex2',
                'enabled' => true,
                'role' => 'ROLE_USER',
                'firstname' => 'Alexandre',
                'lastname'  => 'Canivez',
                'civility'   => 'Mr',
                'created_at' => new \DateTime(),
            ),
            array(
                'username' => 'Alex3',
                'email' => 'alex3@gmail.com',
                'password' => 'alex3',
                'enabled' => true,
                'role' => 'ROLE_USER',
                'firstname' => 'Alexandre',
                'lastname'  => 'Canivez',
                'civility'   => 'Mr',
                'created_at' => new \DateTime(),
            ),
        );

        foreach ($tab as $row)
        {
//            // Get our userManager, you must implement `ContainerAwareInterface`
//            $userManager = $this->container->get('fos_user.user_manager');
//
//            // Create our user and set details
//            $user = $userManager->createUser();
            $user = new User();
            $user->setUsername($row['username']);
            $user->setEmail($row['email']);
            $user->setPlainPassword($row['password']);
            //$user->setPassword('3NCRYPT3D-V3R51ON');
            $user->setEnabled($row['enabled']);
            $user->setRoles(array($row['role']));
            $user->setFirstname($row['firstname']);
            $user->setLastname($row['lastname']);
            $user->setCivility($row['civility']);
            $user->setCreatedAt($row['created_at']);

            // Update the user
//            $userManager->updateUser($user, true);
            $manager->persist($user);

            $this->addReference($row['username'], $user);

        }
        $manager->flush();


    }
    public function getOrder()
    {
        return 3;
    }

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;

    }
}