<?php
namespace AC\TestTechniqueBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AC\TestTechniqueBundle\Entity\Movie;


class LoadMovieData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // Liste des choses à ajouter
        $tab = array(
            array(
                'title' => 'Les dents de la mer',
                'directorname' => 'steven spielberg',
                'date' => new \DateTime('1980/01/01'),
            ),
            array(
                'title' => 'Aliens',
                'directorname' => 'Ridley Scott',
                'date' => new \DateTime('1986/01/01'),
            ),
            array(
                'title' => 'Retour vers le futur',
                'directorname' => 'Robert Zemeckis',
                'date' => new \DateTime('1985/01/01'),
            ),
            array(
                'title' => 'La ligne verte',
                'directorname' => 'Frank Darabont',
                'date' => new \DateTime('2000/01/01'),
            ),
            array(
                'title' => 'Forrest Gump',
                'directorname' => 'Robert Zemeckis',
                'date' => new \DateTime('1994/01/01'),
            ),
            array(
                'title' => 'Rocky',
                'directorname' => 'John G. Avildsen',
                'date' => new \DateTime('1977/01/01'),
            ),
            array(
                'title' => 'Rambo',
                'directorname' => 'Ted Kotcheff',
                'date' => new \DateTime('1998/01/01'),
            ),
            array(
                'title' => 'Coach Carter',
                'directorname' => 'Thomas Carter',
                'date' => new \DateTime('2005/01/01'),
            ),
            array(
                'title' => 'Rédemption',
                'directorname' => 'Phil Joanou',
                'date' => new \DateTime('2006/01/01'),
            ),
            array(
                'title' => 'Thor',
                'directorname' => 'Kenneth Branagh',
                'date' => new \DateTime('2011/01/01'),
            ),
            array(
                'title' => 'Captain America',
                'directorname' => 'Joe Johnston',
                'date' => new \DateTime('2011/01/01'),
            ),
        );

        foreach ($tab as $row)
        {
            $movie = new Movie();
            $movie->setTitle($row['title']);
            $movie->setDirectorname($row['directorname']);
            $movie->setReleasedate($row['date']);

            // On déclenche l'enregistrement
            $manager->persist($movie);
            $this->addReference($row['title'], $movie);

        }
        // On la persiste
        $manager->flush();

    }

    public function getOrder()
    {
        return 1;
    }
}