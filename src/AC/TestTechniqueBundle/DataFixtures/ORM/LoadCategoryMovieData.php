<?php
namespace AC\TestTechniqueBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AC\TestTechniqueBundle\Entity\MovieCategory;


class LoadMovieCategoryData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // Liste des choses � ajouter
        $tab = array(
            array(
                'name' => 'Action',
            ),
            array(
                'name' => 'Aventure',
            ),
            array(
                'name' => 'Fiction',
            ),
            array(
                'name' => 'Horreur',
            ),
            array(
                'name' => 'Romance',
            ),
        );

        foreach ($tab as $row)
        {
            $movieCategory = new MovieCategory();
            $movieCategory->setName($row['name']);

            // On d�clenche l'enregistrement
            $manager->persist($movieCategory);

        }
        // On la persiste
        $manager->flush();

    }

    public function getOrder()
    {
        return 2;
    }
}