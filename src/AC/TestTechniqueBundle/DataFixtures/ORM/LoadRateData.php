<?php
namespace AC\TestTechniqueBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AC\TestTechniqueBundle\Entity\Rate;


class LoadRateData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // Liste des choses � ajouter
        $tab = array(
            array(
                'rate' => 4,
                'user' => $this->getReference('admin'),
                'movie' => $this->getReference('Thor'),
            ),
            array(
                'rate' => 6,
                'user' => $this->getReference('Alex3'),
                'movie' => $this->getReference('Thor'),
            ),
            array(
                'rate' => 6,
                'user' => $this->getReference('Alex'),
                'movie' => $this->getReference('Captain America'),
            ),
            array(
                'rate' => 3,
                'user' => $this->getReference('admin'),
                'movie' => $this->getReference('Captain America'),
            ),
            array(
                'rate' => 9,
                'user' => $this->getReference('admin'),
                'movie' => $this->getReference('Forrest Gump'),
            ),
            array(
                'rate' => 8,
                'user' => $this->getReference('admin'),
                'movie' => $this->getReference('Aliens'),
            ),
            array(
                'rate' => 8,
                'user' => $this->getReference('Alex'),
                'movie' => $this->getReference('Aliens'),
            ),
            array(
                'rate' => 10,
                'user' => $this->getReference('admin'),
                'movie' => $this->getReference('La ligne verte'),
            ),
            array(
                'rate' => 4,
                'user' => $this->getReference('Alex3'),
                'movie' => $this->getReference('La ligne verte'),
            ),
            array(
                'rate' => 7,
                'user' => $this->getReference('Alex2'),
                'movie' => $this->getReference('Thor'),
            ),
            array(
                'rate' => 4,
                'user' => $this->getReference('Alex2'),
                'movie' => $this->getReference('Aliens'),
            ),
            array(
                'rate' => 2,
                'user' => $this->getReference('Alex2'),
                'movie' => $this->getReference('Aliens'),
            ),
            array(
                'rate' => 10,
                'user' => $this->getReference('Alex2'),
                'movie' => $this->getReference('Captain America'),
            ),
            array(
                'rate' => 4,
                'user' => $this->getReference('admin'),
                'movie' => $this->getReference('Rambo'),
            ),
            array(
                'rate' => 8,
                'user' => $this->getReference('Alex'),
                'movie' => $this->getReference('Rambo'),
            ),
            array(
                'rate' => 10,
                'user' => $this->getReference('Alex3'),
                'movie' => $this->getReference('Rambo'),
            ),
        );

        foreach ($tab as $row)
        {
            $rate = new Rate();
            $rate->setRate($row['rate']);
            $rate->setUser($row['user']);
            $rate->setMovie($row['movie']);

            // On d�clenche l'enregistrement
            $manager->persist($rate);
        }
        // On la persiste
        $manager->flush();

    }

    public function getOrder()
    {
        return 4;
    }
}