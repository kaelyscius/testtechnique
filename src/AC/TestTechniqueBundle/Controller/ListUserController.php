<?php

namespace AC\TestTechniqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ListUserController extends Controller
{
    public function indexAction()
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ACTestTechniqueBundle:User')
        ;
        $listUsers = $repository->findAll();

        $countRate = $this->getDoctrine()
            ->getManager()
            ->getRepository('ACTestTechniqueBundle:Rate')
            ->getUserCountRate();



        return $this->render('ACTestTechniqueBundle:AcTest:list_users.html.twig',
            array(
                'users' => $listUsers,
                'countRates' => $countRate,
            )
        );
    }
}
