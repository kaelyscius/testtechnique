<?php

namespace AC\TestTechniqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListMovieController extends Controller
{
    public function indexAction()
    {
        $averageRates = $this->getDoctrine()
            ->getManager()
            ->getRepository('ACTestTechniqueBundle:Rate')
            ->getMovieWithAverageRate();

        return $this->render('ACTestTechniqueBundle:AcTest:layout.html.twig',
            array(
                'movies' => $averageRates,
                )
        );
    }
}
