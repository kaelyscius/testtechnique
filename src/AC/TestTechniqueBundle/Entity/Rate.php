<?php

namespace AC\TestTechniqueBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Rate
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AC\TestTechniqueBundle\Entity\RateRepository")
 */
class Rate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var user
     * @ORM\ManyToOne(targetEntity="User", inversedBy="rates", cascade={"remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var movie
     *
     * @ORM\ManyToOne(targetEntity="Movie", inversedBy="rates", cascade={"remove"})
     * @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
     */
    private $movie;

    /**
     * @var integer
     *
     * @ORM\Column(name="rate", type="integer")
     */
    protected $rate;

    public function __construct()
    {
        $this->movie = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param int $rate
     * @return Rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param $user
     *
     * @return Rate
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set Movie
     *
     * @param $Movie
     *
     * @return Rate
     */
    public function setMovie(Movie $movie)
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * Get Movie
     *
     * @return string
     */
    public function getMovie()
    {
        return $this->movie;
    }

}

