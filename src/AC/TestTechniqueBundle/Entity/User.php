<?php

namespace AC\TestTechniqueBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="ac_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="firstname", type="string", length=60)
     */
    protected $firstName;

    /**
     * @ORM\Column(name="lastname", type="string", length=100)
     */
    protected $lastName;

    /**
     * @ORM\Column(name="civility", type="string", columnDefinition="ENUM('Mr', 'Melle', 'Mme')")
     */
    protected $civility;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity="AC\TestTechniqueBundle\Entity\MovieCategory", cascade={"persist"})
     */
    protected $movieCategories;

    /**
     * @ORM\OneToMany(targetEntity="Rate", mappedBy="user", cascade={"remove", "persist"})
     */
    protected $rates;

    /**
     * @ORM\OneToOne(targetEntity="Picture", cascade={"remove", "persist"})
     * @ORM\JoinColumn(name="picture_id", referencedColumnName="id")
     */
    private $picture;

    public function __construct()
    {
        parent::__construct();
        $this->movieCategories = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return mixed
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * @param mixed $rates
     * @return User
     */
    public function setRates($rates)
    {
        $this->rates = $rates;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     * @return User
     */
    public function setPicture(Picture $picture)
    {
        $this->picture = $picture;
        return $this;
    }

    public function addCategory(MovieCategory $category)
    {
        // Ici, on utilise l'ArrayCollection vraiment comme un tableau
        $this->categories[] = $category;

        return $this;
    }

    public function removeCategory(MovieCategory $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * @return mixed
     */
    public function getMovieCategories()
    {
        return $this->movieCategories;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * Set civility
     *
     * @param string $civility
     *
     * @return User
     */
    public function setCivility($civility)
    {
        $this->civility = $civility;

        return $this;
    }

    /**
     * Get civility
     *
     * @return string
     */
    public function getCivility()
    {
        return $this->civility;
    }

    /**
     * Add movieCategory
     *
     * @param \AC\TestTechniqueBundle\Entity\MovieCategory $movieCategory
     *
     * @return User
     */
    public function addMovieCategory(\AC\TestTechniqueBundle\Entity\MovieCategory $movieCategory)
    {
        $this->movieCategories[] = $movieCategory;

        return $this;
    }

    /**
     * Remove movieCategory
     *
     * @param \AC\TestTechniqueBundle\Entity\MovieCategory $movieCategory
     */
    public function removeMovieCategory(\AC\TestTechniqueBundle\Entity\MovieCategory $movieCategory)
    {
        $this->movieCategories->removeElement($movieCategory);
    }
}
