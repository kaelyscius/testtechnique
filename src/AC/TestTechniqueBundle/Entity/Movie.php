<?php

namespace AC\TestTechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Movie
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AC\TestTechniqueBundle\Entity\MovieRepository")
 */
class Movie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="directorname", type="string", length=255)
     */
    private $directorname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="releasedate", type="date")
     */
    private $releasedate;

    /**
     * @ORM\OneToMany(targetEntity="Rate", mappedBy="movie", cascade={"remove", "persist"})
     */
    protected $rate;

    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param mixed $rate
     * @return Movie
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return movie
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set directorname
     *
     * @param string $directorname
     *
     * @return Movie
     */
    public function setDirectorname($directorname)
    {
        $this->directorname = $directorname;

        return $this;
    }

    /**
     * Get directorname
     *
     * @return string
     */
    public function getDirectorname()
    {
        return $this->directorname;
    }

    /**
     * Set releasedate
     *
     * @param \DateTime $releasedate
     *
     * @return Movie
     */
    public function setReleasedate($releasedate)
    {
        $this->releasedate = $releasedate;

        return $this;
    }

    /**
     * Get releasedate
     *
     * @return \DateTime
     */
    public function getReleasedate()
    {
        return $this->releasedate;
    }

}

