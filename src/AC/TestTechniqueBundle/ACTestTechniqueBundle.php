<?php

namespace AC\TestTechniqueBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ACTestTechniqueBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
